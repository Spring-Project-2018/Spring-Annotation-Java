package coid.bca.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coid.bca.repository.AuditTrailRepository;
import coid.bca.repository.BranchRepository;

@Service
public class BranchService {
	
	private AuditTrailRepository auditTrailRepository;
	private BranchRepository branchRepository;
	
	public void save() {
		auditTrailRepository.save("Save Branch");
		branchRepository.save();
	}
	
	public void edit() {
		auditTrailRepository.save("Edit Branch");
		branchRepository.edit();
	}
	
	public void delete() {
		auditTrailRepository.save("Delete Branch");
		branchRepository.delete();
	}
	
	@Autowired
	public void setAuditTrailRepository(AuditTrailRepository auditTrailRepository) {
		this.auditTrailRepository = auditTrailRepository;
	}
	
	@Autowired
	public void setBranchRepository(BranchRepository branchRepository) {
		this.branchRepository = branchRepository;
	}
	
}
