package coid.bca;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import coid.bca.repository.AuditTrailRepository;
import coid.bca.repository.BranchRepository;
import coid.bca.service.BranchService;

@Configuration
@ComponentScan(basePackages={"coid.bca.repository", "coid.bca.service"})
public class AppConfig {

	public BranchService branchService() {
		// 1. Constructor
		// BranchService branchService = new BranchService(new AuditTrailRepository(), new BranchRepository());
		
		// 2. Setter
		BranchService branchService = new BranchService();
		branchService.setAuditTrailRepository(new AuditTrailRepository());
		branchService.setBranchRepository(new BranchRepository());
		
		return branchService;
	}
	
}
