package coid.bca;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import coid.bca.service.BranchService;

public class Main {

	public static void main(String[] args) {

		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		
		BranchService branchService = (BranchService) context.getBean("branchService");
		
		branchService.save();
		branchService.edit();
		branchService.delete();

	}

}
